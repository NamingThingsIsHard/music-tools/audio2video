/**
Bars drawn on the screen edges.
They will take up a certain percentag of the screen starting from the screen edge.

There can be:

 - bars on given sides
 - bars wandering around the sides of the screen

*/
