/**
  * This sketch demonstrates how to play a file with Minim using an AudioPlayer. <br />
  * It's also a good example of how to draw the waveform of the audio. Full documentation 
  * for AudioPlayer can be found at http://code.compartmental.net/minim/audioplayer_class_audioplayer.html
  * <p>
  * For more information about Minim and additional features, 
  * visit http://code.compartmental.net/minim/
  */

import ddf.minim.analysis.*;
import ddf.minim.*;
import com.hamoid.*;

VideoExport videoExport;
int movieFPS = 25;

Minim minim;
AudioPlayer player;
float soundDurationSeconds;

FFT fft;

String audioFileName;

/**
Base class for visualizations
*/
abstract class Visualization {
  float maxSize;
  int specCount;
  
  Visualization(int specCount){
    this.specCount = specCount;
  }
  
  abstract void draw(float[] amplitudes);
}

Visualization viz;

void setup()
{
  size(500, 500);

  String[] files = loadStrings("files.txt");
  audioFileName = files[2];
  
  // we pass this to Minim so that it can load files from the data directory
  minim = new Minim(this);
  
  // loadFile will look in all the same places as loadImage does.
  // this means you can find files that are in the data folder and the 
  // sketch folder. you can also pass an absolute path, or a URL.
  player = minim.loadFile(audioFileName);
  soundDurationSeconds = player.length() / 1000f;
  
  fft = new FFT(player.bufferSize(), player.sampleRate());
  fft.logAverages(172,10);
  //fft.linAverages(100);
  
  viz = new CircleBarsViz(80);
  videoExport = new VideoExport(this, "what.mp4");
  videoExport.setFrameRate(movieFPS);
  frameRate(movieFPS);
  //videoExport.setAudioFileName(audioFileName);
  //videoExport.startMovie();
}

void draw()
{
  if(! player.isPlaying()){
    player.play();
  }
  background(0);
  stroke(255);
  
   fft.forward(player.mix);
   //float[] amplitudes = new float[fft.specSize()];
   //for (int i = 0; i < specs; i++){
   //  amplitudes[i] = fft.getBand(i);
   //}
   float[] amplitudes = new float[fft.avgSize()];
   for (int i = 0; i < amplitudes.length; i++){
     amplitudes[i] = fft.getAvg(i);
   }
   
   viz.draw(amplitudes);
  
  // draw a line to show where in the song playback is currently located
  float posx = map(player.position(), 0, player.length(), 0, width);
  colorMode(RGB);
  stroke(0,200,0);
  strokeWeight(10);
  line(0, height-5, posx, height-5);
  videoExport.saveFrame();
  
  // End when we have exported enough frames 
  // to match the sound duration.
  if(frameCount > round(movieFPS * soundDurationSeconds)) {
    videoExport.endMovie();
    exit();
  }  
} 

void keyPressed(){
 if (key == 'q') {
    videoExport.endMovie();
    exit();
  }
}  
