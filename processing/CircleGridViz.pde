int get_max_els(float max_size){
  return floor(width / max_size) * floor(height / max_size); 
}

/**
Still needs some work :/
80 items on a 1920 x 1080 screen
*/
float getMaxSize(int count){
    float max_size = min(width, height);
    int max_els = get_max_els(max_size);
    while(max_els < count){
        max_size--;
        max_els = get_max_els(max_size);
    }     

    //columns = width / max_size
    //rows = height / max_size

    return max_size;
}

class CircleGridViz extends Visualization {
  
  float step;
  float maxRadius;
  float colorPhase;
  float framePhaseStep;
  
  CircleGridViz(int specCount, float framePhaseStep){
    super(specCount);
    step = getMaxSize(this.specCount);
    maxRadius = step / 2;
    colorPhase = 0;
    this.framePhaseStep = framePhaseStep;
  }
  
  CircleGridViz(int specCount){
    this(specCount, 5f);
  }
  
  void draw(float[] amplitudes){
   float x = step;
   float y = step;
   float maxAmp = max(amplitudes);
   if(maxAmp < 1) {
     return;
   }
   //int c = 0;
   colorMode(HSB);
   colorPhase = (colorPhase + framePhaseStep) % 255;
   for (float amp : amplitudes){
     float radius = map(amp, 0, maxAmp, 0, maxRadius);
     
     float a = (map(amp, 0, maxAmp, 0, 255) + colorPhase) % 255;
     fill(a,255,255);
     noStroke();
     ellipse(x-maxRadius, y-maxRadius, radius, radius);
     if(x + maxRadius >= width){
       x = step;
       y += step;
     } else {
       x += step;
     }
   }  
  }
}
