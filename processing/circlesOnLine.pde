void circlesOnLine(){
   int specs = fft.specSize();
   float xStep = width / specs;
   float xCenter = xStep;
   float yCenter = height / 2;
   float maxRadius = xStep / 2;
   
   for (int i = 0; i < specs; i++){
     // draw the line for frequency band i, scaling it by 4 so we can see it a bit better
     //line(i, height, i, height - fft.getBand(i) * 4);
     float radius = map(fft.getBand(i), 0, 100, 0, maxRadius); 
     ellipse(xCenter, yCenter, radius, radius);
     xCenter += xStep;
   }
  
}
