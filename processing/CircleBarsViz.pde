/**

Bars on a cirle.

The bars can either be static on the circle or wander around it.

*/

class CircleBarsViz extends Visualization {
  int circlePoints; // How many points to draw in to total to make a circle
  float degreeStep; // How many degrees we move forward when drawing a point on the circle
  int specCadence; // How often a spec should be drawn
  float spirals;
  float minSpirals;
  float maxSpirals;
  float totalDegrees;
  float spiralChangePerFrame = 0.02;
  
  CircleBarsViz(int specCount, int minCirclePoints){
    super(specCount);
    minSpirals = 5;
    maxSpirals = 10;
    spirals = min(minSpirals, maxSpirals); // TODO pass this as a param
    circlePoints = max(specCount, minCirclePoints);
    specCadence = ceil((float)circlePoints / (float) specCount);
    calcStep();
  }
  
  CircleBarsViz(int specCount){
    this(specCount, 360);
  }
  
  void calcStep(){
    totalDegrees = 360 * spirals;
    degreeStep = totalDegrees / (float) circlePoints;
  }
  
  void draw(float[] amplitudes){
    
    // To move to the center of the screen
    float translateX = width / 2; //<>// //<>//
    float translateY = height / 2;
    
    float dominantSide = min(width, height);
    float radius = dominantSide / 3.0;
    float maxRadiusIncrease = radius / 2.0;
    
    //float startX = (radius * cos(0)) + translateX;
    //float startY = (radius * cos(0)) + translateY;
    
    strokeWeight(1);
    noFill();
    beginShape();
    //curveVertex(startX, startY);
    
    if(spiralChangePerFrame != 0){
      float tempSpirals = spirals + spiralChangePerFrame;
      if( tempSpirals > maxSpirals || tempSpirals < minSpirals){
        spiralChangePerFrame = - spiralChangePerFrame;
      }       
      spirals += spiralChangePerFrame;
      calcStep();
    }
    
    int counter = 0;
    int specIndex = 0;
    float degree=0; //<>//

    pushMatrix();

    // Rotate the circle at each frame a little
    float frameInSecond = frameCount % 360;
    float anglePerFrame = 1; // TODO make this configurable with rotationsPerSecond
    float rotation = radians(frameInSecond * anglePerFrame);

    translate(translateX, translateY);
    rotate(rotation);

    while(degree <= totalDegrees+1){
      float rad = radians(degree);
      float radi = radius;
      if(spirals >1 ){
        radi -= map(degree,0,totalDegrees,0, radius);
      }
      
      // Draw a datapoint
      if(specIndex <amplitudes.length && counter % specCadence == 0 ){
        radi += map(amplitudes[specIndex],0, 128, 0, maxRadiusIncrease);
        specIndex++;
      }
      curveVertex(
        (radi * cos(rad)),
        (radi * sin(rad))
      );
      degree += degreeStep;
      counter++;
    }
    endShape();
    popMatrix();
  }
}
